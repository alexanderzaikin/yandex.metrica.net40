﻿// Decompiled with JetBrains decompiler
// Type: Yandex.Metrica.Providers.FingerprintDataProvider
// Assembly: Yandex.Metrica.NET, Version=3.5.1.0, Culture=neutral, PublicKeyToken=21e4d3bd28ff137d
// MVID: 30D77F94-06C4-410D-9A5A-E6909B7FCAB1
// Assembly location: C:\Users\Alexander\Documents\Visual Studio 2017\Projects\ConsoleApp1\packages\Yandex.Metrica.3.5.1\lib\net45\Yandex.Metrica.NET.dll

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Yandex.Metrica.Aides;
using Yandex.Metrica.Patterns;

namespace Yandex.Metrica.Providers
{
  internal class FingerprintDataProvider : ADataProvider<Task<string>>
  {
    private static async Task<string> GetDeviceFingerprint()
    {
      string publisherApps = FingerprintDataProvider.GetPublisherApps();
      ulong bootTime = DateTime.UtcNow.ToUnixTime() - (ulong) Environment.TickCount / 1000UL;
      FingerprintDataProvider.DiskSpace diskSpace = await FingerprintDataProvider.GetDiskSpace();
      ulong? nullable1;
      if (diskSpace == null)
      {
        nullable1 = new ulong?();
      }
      else
      {
        ulong? totalSpace = diskSpace.TotalSpace;
        ulong num = 1024;
        nullable1 = totalSpace.HasValue ? new ulong?(totalSpace.GetValueOrDefault() / num) : new ulong?();
      }
      ulong? nullable2 = nullable1;
      ulong? nullable3;
      if (diskSpace == null)
      {
        nullable3 = new ulong?();
      }
      else
      {
        ulong? freeSpace = diskSpace.FreeSpace;
        ulong num = 1024;
        nullable3 = freeSpace.HasValue ? new ulong?(freeSpace.GetValueOrDefault() / num) : new ulong?();
      }
      ulong? nullable4 = nullable3;
      string format = "{{\n    \"dfid\": {{\n        \"tds\": {0},\n        \"fds\": {1},\n        \"boot_time\": {2},\n        \"apps\": {{\n                \"version\": 0,\n                \"names\": [{3}]\n                }}\n    }}\n}}";
      object[] objArray = new object[4];
      int index1 = 0;
      ulong? nullable5 = nullable2;
      // ISSUE: variable of a boxed type
     var local1 = (ValueType) (ulong) (nullable5.HasValue ? (long) nullable5.GetValueOrDefault() : 0L);
      objArray[index1] = (object) local1;
      int index2 = 1;
      nullable5 = nullable4;
      // ISSUE: variable of a boxed type
        var local2 = (ValueType) (ulong) (nullable5.HasValue ? (long) nullable5.GetValueOrDefault() : 0L);
      objArray[index2] = (object) local2;
      objArray[2] = (object) bootTime;
      objArray[3] = (object) publisherApps;
      return string.Format(format, objArray);
    }

    private static string GetPublisherApps()
    {
      List<string> source;
      try
      {
        source = new List<string>();
      }
      catch
      {
        source = new List<string>();
      }
      if (source.Count <= 0)
        return string.Empty;
      return "\"" + source.Aggregate<string>((Func<string, string, string>) ((a, b) => a + "\", \"" + b)) + "\"";
    }

    private static async Task<FingerprintDataProvider.DiskSpace> GetDiskSpace()
    {
      try
      {
        ulong? nullable1 = new ulong?();
        ulong? nullable2 = new ulong?();
        nullable1 = new ulong?(0UL);
        ulong? nullable3 = nullable1;
        foreach (DriveInfo drive in DriveInfo.GetDrives())
        {
          if (drive.IsReady && drive.DriveType == DriveType.Fixed)
          {
            ulong? nullable4 = nullable1;
            ulong availableFreeSpace = (ulong) drive.AvailableFreeSpace;
            nullable1 = nullable4.HasValue ? new ulong?(nullable4.GetValueOrDefault() + availableFreeSpace) : new ulong?();
            ulong? nullable5 = nullable3;
            ulong totalSize = (ulong) drive.TotalSize;
            nullable3 = nullable5.HasValue ? new ulong?(nullable5.GetValueOrDefault() + totalSize) : new ulong?();
          }
        }
        return new FingerprintDataProvider.DiskSpace()
        {
          TotalSpace = nullable3,
          FreeSpace = nullable1
        };
      }
      catch (Exception ex)
      {
        return (FingerprintDataProvider.DiskSpace) null;
      }
    }

    protected override Task<string> ProvideOrThrowException()
    {
      return FingerprintDataProvider.GetDeviceFingerprint();
    }

    private class DiskSpace
    {
      public ulong? FreeSpace { get; set; }

      public ulong? TotalSpace { get; set; }
    }
  }
}
