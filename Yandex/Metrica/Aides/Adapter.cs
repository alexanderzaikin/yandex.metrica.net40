﻿// Decompiled with JetBrains decompiler
// Type: Yandex.Metrica.Aides.Adapter
// Assembly: Yandex.Metrica.NET, Version=3.5.1.0, Culture=neutral, PublicKeyToken=21e4d3bd28ff137d
// MVID: 30D77F94-06C4-410D-9A5A-E6909B7FCAB1
// Assembly location: C:\Users\Alexander\Documents\Visual Studio 2017\Projects\ConsoleApp1\packages\Yandex.Metrica.3.5.1\lib\net45\Yandex.Metrica.NET.dll

using System;
using System.Reflection;
using System.Text;

namespace Yandex.Metrica.Aides
{
  internal static class Adapter
  {
    public static Type GetTypeInfo(this Type type)
    {
      return type;
    }

    public static Assembly GetEntryAssembly()
    {
      return Assembly.GetEntryAssembly();
    }

    public static byte[] ExtractData(object o)
    {
      UnhandledExceptionEventArgs exceptionEventArgs = o as UnhandledExceptionEventArgs;
      if (exceptionEventArgs != null)
        return Encoding.UTF8.GetBytes(exceptionEventArgs.ExceptionObject.ToString());
      return (byte[]) null;
    }

    public static bool IsInternalException(object o)
    {
      UnhandledExceptionEventArgs exceptionEventArgs = o as UnhandledExceptionEventArgs;
      if (exceptionEventArgs != null)
        return exceptionEventArgs.ExceptionObject.ToString().Contains("Yandex.Metrica");
      return false;
    }

    public static void TryHandleException(object o)
    {
    }
  }
}
