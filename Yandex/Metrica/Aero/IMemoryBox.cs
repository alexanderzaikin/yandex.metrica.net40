﻿// Decompiled with JetBrains decompiler
// Type: Yandex.Metrica.Aero.IMemoryBox
// Assembly: Yandex.Metrica.NET, Version=3.5.1.0, Culture=neutral, PublicKeyToken=21e4d3bd28ff137d
// MVID: 30D77F94-06C4-410D-9A5A-E6909B7FCAB1
// Assembly location: C:\Users\Alexander\Documents\Visual Studio 2017\Projects\ConsoleApp1\packages\Yandex.Metrica.3.5.1\lib\net45\Yandex.Metrica.NET.dll

namespace Yandex.Metrica.Aero
{
  internal interface IMemoryBox
  {
    void Keep<TItem>(TItem item, string key = null);

    bool Check<TItem>(string key = null);

    void Destroy<TItem>(string key = null);

    TItem Revive<TItem>(string key = null, params object[] constructorArgs);
  }
}
