﻿// Decompiled with JetBrains decompiler
// Type: SilentOrbit.ProtocolBuffers.ThreadSafeStack
// Assembly: Yandex.Metrica.NET, Version=3.5.1.0, Culture=neutral, PublicKeyToken=21e4d3bd28ff137d
// MVID: 30D77F94-06C4-410D-9A5A-E6909B7FCAB1
// Assembly location: C:\Users\Alexander\Documents\Visual Studio 2017\Projects\ConsoleApp1\packages\Yandex.Metrica.3.5.1\lib\net45\Yandex.Metrica.NET.dll

using System;
using System.Collections.Generic;
using System.IO;

namespace SilentOrbit.ProtocolBuffers
{
  public class ThreadSafeStack : MemoryStreamStack, IDisposable
  {
    private Stack<MemoryStream> stack = new Stack<MemoryStream>();

    public MemoryStream Pop()
    {
      lock (this.stack)
      {
        if (this.stack.Count == 0)
          return new MemoryStream();
        return this.stack.Pop();
      }
    }

    public void Push(MemoryStream stream)
    {
      lock (this.stack)
        this.stack.Push(stream);
    }

    public void Dispose()
    {
      lock (this.stack)
        this.stack.Clear();
    }
  }
}
