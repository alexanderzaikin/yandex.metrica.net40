﻿// Decompiled with JetBrains decompiler
// Type: SilentOrbit.ProtocolBuffers.PositionStream
// Assembly: Yandex.Metrica.NET, Version=3.5.1.0, Culture=neutral, PublicKeyToken=21e4d3bd28ff137d
// MVID: 30D77F94-06C4-410D-9A5A-E6909B7FCAB1
// Assembly location: C:\Users\Alexander\Documents\Visual Studio 2017\Projects\ConsoleApp1\packages\Yandex.Metrica.3.5.1\lib\net45\Yandex.Metrica.NET.dll

using System;
using System.IO;

namespace SilentOrbit.ProtocolBuffers
{
  public class PositionStream : Stream
  {
    private Stream stream;

    public int BytesRead { get; private set; }

    public PositionStream(Stream baseStream)
    {
      this.stream = baseStream;
    }

    public override void Flush()
    {
      throw new NotImplementedException();
    }

    public override int Read(byte[] buffer, int offset, int count)
    {
      int num = this.stream.Read(buffer, offset, count);
      this.BytesRead += num;
      return num;
    }

    public override int ReadByte()
    {
      int num = this.stream.ReadByte();
      ++this.BytesRead;
      return num;
    }

    public override long Seek(long offset, SeekOrigin origin)
    {
      if (this.stream.CanSeek)
        return this.stream.Seek(offset, origin);
      if (origin != SeekOrigin.Current || offset < 0L)
        throw new NotImplementedException();
      byte[] buffer = new byte[Math.Min(offset, 10000L)];
      long num1 = (long) this.BytesRead + offset;
      while ((long) this.BytesRead < num1)
      {
        int num2 = this.stream.Read(buffer, 0, (int) Math.Min(num1 - (long) this.BytesRead, (long) buffer.Length));
        if (num2 != 0)
          this.BytesRead += num2;
        else
          break;
      }
      return (long) this.BytesRead;
    }

    public override void SetLength(long value)
    {
      throw new NotImplementedException();
    }

    public override void Write(byte[] buffer, int offset, int count)
    {
      throw new NotImplementedException();
    }

    public override bool CanRead
    {
      get
      {
        return true;
      }
    }

    public override bool CanSeek
    {
      get
      {
        return false;
      }
    }

    public override bool CanWrite
    {
      get
      {
        return false;
      }
    }

    public override long Length
    {
      get
      {
        return this.stream.Length;
      }
    }

    public override long Position
    {
      get
      {
        return (long) this.BytesRead;
      }
      set
      {
        throw new NotImplementedException();
      }
    }

    protected override void Dispose(bool disposing)
    {
      this.stream.Dispose();
      base.Dispose(disposing);
    }
  }
}
